import java.util.Random;

public class Spieler {
	
	private String name;
	private int runde;
	private int wurf;
	private Runde[] bowlingspiel = new Runde[10];
	//private Runde10 runde10 = new Runde10(9, new Spieler(""));
	
	Runde10 runde10 = new Runde10(9, this);
	Random rand = new Random();
	
	//Runde[] bowlingspiel = new Runde[10];
	
	
	public Spieler(String s) {
		// TODO Auto-generated constructor stub
		this.name = s;
		
		;
		
	}

	public int kugelwerfen()
	{
		int wurf = rand.nextInt(11);
		
		return wurf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Runde[] getRunden() {
		return bowlingspiel;
	}
	
	public int getPunkte()
	{
		
		return 0;
	}
	
	public void playGame()
	{
		for(int i = 0; i < this.getRunden().length; i++)
		{
			if(i < 9)
			{
				this.getRunden()[i] = new Runde(i, this);
				
				char currentWurf1 = (char)this.kugelwerfen();
				char currentWurf2 = (char)this.kugelwerfen();
				
				
				if(currentWurf1 + currentWurf2 > 10)
				{
					currentWurf2 = (char) (10 - currentWurf1);
				}
				
				
				/*if((currentWurf1 < 10) && (currentWurf1 + currentWurf2 == 10))
				{
					currentWurf2 = '/';
				}*/
				
				/*if(currentWurf1 == 10)
				{
					currentWurf1 = 'X';
					currentWurf2 = ' ';
				}*/
				
				this.getRunden()[i].setWurf1(currentWurf1);
				this.getRunden()[i].setWurf2(currentWurf2);
			}
			
			else
			{
				this.getRunden()[i] = runde10;
				
				char currentWurf1 = (char)this.kugelwerfen();
				char currentWurf2 = (char)this.kugelwerfen();
				
				if(currentWurf1 == 10 && currentWurf2 == 10)
				{
					runde10.setWurf3((char)this.kugelwerfen());
				}
				
				else if(currentWurf1 + currentWurf2 >= 10)
				{
					currentWurf2 = (char) (10 - currentWurf1);
					runde10.setWurf3((char)this.kugelwerfen());
				}
				
				this.getRunden()[i].setWurf1(currentWurf1);
				this.getRunden()[i].setWurf2(currentWurf2);
			}
			
		}
	}
	
	public void printGame()
	{
		System.out.println(this.name);
		int totalpoints = 0;
		System.out.print("Runde:   ");
		//Rundenzahl ausgeben
		for(int i = 0; i < this.getRunden().length; i++)
		{
			
			if((int)this.getRunden()[i].getWurf1() < 10)
				System.out.print(" " + (i + 1) + "    ");
			else
				System.out.print("  " + (i + 1) + "    ");	
		}
		
		System.out.println("");
		System.out.print("Wurf1/2: ");
		//Wurf1 und Wurf2 ausgeben von Rund 1 bis 9
		for(int i = 0; i < this.getRunden().length - 1; i++)
		{
			if(this.getRunden()[i].getRundenPunkte() < 10) //Runde ohne Spare und Strike
			{
				System.out.print((int)this.getRunden()[i].getWurf1() + "|" + (int)this.getRunden()[i].getWurf2() + "   ");
				
			}
			
			else if(this.getRunden()[i].getWurf1() == 10) //Runde mit Strike
			{
					System.out.print("X|" + "    ");
				
			}
			
			else if(this.getRunden()[i].getWurf1() < 10 && this.getRunden()[i].getRundenPunkte() == 10)//Runde mit Spare
			{
					System.out.print((int)this.getRunden()[i].getWurf1() + "|/" + "   ");
					
			}
				
		}
		
		//Runde 10 ausgeben
		if(runde10.getWurf1() == 10 && runde10.getWurf2() == 10 && runde10.getWurf3() == 10) //alle 3 W�rfe Strike
		{
			System.out.print("X|X|X");
		}
		else if(runde10.getWurf1() == 10 && runde10.getWurf2() == 10) // Wurf1 und Wurf2 Strike
		{
			System.out.print("X|X|" + (int)runde10.getWurf3());
		}
		else if(runde10.getWurf1() == 10 && ((runde10.getWurf2() + runde10.getWurf3()) == 10)) //Wurf1 Strike Wurf2 + Wurf3 Spare
		{
			System.out.print("X|" + (int)runde10.getWurf2() + "|/");
		}
		else if(runde10.getWurf1() == 10 && ((runde10.getWurf2() + runde10.getWurf3()) < 10)) // Wurf1 Strike danach Wurf2 und Wurf3
		{
			System.out.print("X|" + (int)runde10.getWurf2() + "|" + (int)runde10.getWurf3());
		}
		else if(runde10.getWurf1() < 10 &&  runde10.getRundenPunkte() == 10 && runde10.getWurf3() == 10) //Wurf1 + Wurf2 Spare Wurf3 Strike
		{
			System.out.print((int)runde10.getWurf1() + "|/|X");
		}
		else if(runde10.getWurf1() < 10 &&  runde10.getRundenPunkte() == 10 && runde10.getWurf3() < 10) //Wurf1 + Wurf2 Spare dann Wurf3
		{
			System.out.print((int)runde10.getWurf1() + "|/|" + (int)runde10.getWurf3());
		}
		else if(runde10.getRundenPunkte() < 10) // Kein Spare oder Strike
		{
			System.out.print((int)runde10.getWurf1() + "|" + (int)runde10.getWurf2() + "|");
		}
		
		
		
		System.out.println("");
		System.out.print("Punkte:  ");
		
		//Rundenpunktzahl ausgeben
		for(int i = 0; i < this.getRunden().length; i++)
		{
			int rundenpunkte = 0;
			
			if(this.getRunden()[i].getWurf1() == 10 && i < this.getRunden().length - 1)
			{
			if(this.getRunden()[i + 1].getWurf1() == 10 && i < this.getRunden().length - 2) // Wenn zwei Strikes hintereinander geworfen wurden nur m�glich bis Runde 8
				rundenpunkte = 10 + this.getRunden()[i + 1].getWurf1() + this.getRunden()[i + 2].getWurf1();
				else
				rundenpunkte = 10 + this.getRunden()[i + 1].getRundenPunkte(); // wenn ein Strike geworfen wurde
			}
			else if(this.getRunden()[i].getRundenPunkte() == 10 && i < this.getRunden().length - 1)
			{
				rundenpunkte = 10 + this.getRunden()[i + 1].getWurf1(); // wenn ein Spare geworfen wurde
			}
			else if(this.getRunden()[i].getRundenPunkte() < 10 && i < this.getRunden().length - 1) //kein Strike oder Spare
			{
				rundenpunkte = this.getRunden()[i].getWurf1() + this.getRunden()[i].getWurf2(); 
			}
			else if(i == this.getRunden().length - 1) // 10. Runde werden alle Punkte addiert 
			{
				rundenpunkte = this.getRunden()[i].getWurf1() + this.getRunden()[i].getWurf2() + runde10.getWurf3();
			}
			
			totalpoints += rundenpunkte;
			System.out.print(totalpoints + "    ");
		}
		
	}
}

	