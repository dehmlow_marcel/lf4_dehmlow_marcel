
public class Runde {
	private int nummer;
	private char wurf1;
	private char wurf2;
	private Spieler spieler;
	
	public Runde(int nummer, Spieler spieler) {
		super();
		this.nummer = nummer;
		this.spieler = spieler;
	}

	public char getWurf1() {
		return wurf1;
	}

	public void setWurf1(char wurf1) {
		this.wurf1 = wurf1;
	}

	public char getWurf2() {
		return wurf2;
	}

	public void setWurf2(char wurf2) {
		this.wurf2 = wurf2;
	}

	public int getNummer() {
		return nummer;
	}

	public void setSpieler(Spieler spieler) {
		this.spieler = spieler;
	}
	
	public int getRundenPunkte()
	{
		return (int)this.wurf1 + (int)this.wurf2;
		
	}
	
	
}
