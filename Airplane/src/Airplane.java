import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javafx.scene.transform.Transform;
import java.util.Random;
import java.awt.geom.AffineTransform;


public class Airplane {
	private String registration;
	private String type;
	private String flightNo;
	private Vector3D position;
	private Vector3D directionalVector;
	private boolean isBig;
	private BufferedImage Image;
	private AffineTransform Transform;
	
	
	
	public Airplane(String registration, String type, String flightNo, Vector3D position, Vector3D directionalVector, boolean isBig) {
		super();
		this.registration = registration;
		this.type = type;
		this.flightNo = flightNo;
		this.position = position;
		this.directionalVector = directionalVector;
		
		if(isBig == true) 
		{
			try
			{
				Image = ImageIO.read(this.getClass().getResourceAsStream("airplane_big.png"));
			}
			catch(IOException e)
			{
				
			}
		}
		
		else
		{
			try
			{
				Image = ImageIO.read(this.getClass().getResourceAsStream("airplane_small.png"));
			}
			catch(IOException e)
			{
				
			}
		}
		
		Transform = new AffineTransform();
		
	}
	
	public void fly()
	{
		this.position.setX(this.position.getX() + this.directionalVector.getX());
		this.position.setY(this.position.getY() + this.directionalVector.getY());
		
		if(this.position.getX() > 1280)
		{
			this.position.setX(0);
		}
		else if(this.position.getY() > 1024)
		{
			this.position.setY(0);
		}
		
		else if(this.position.getX() < 0)
		{
			this.position.setX(1280);
		}
		
		else if(this.position.getY() < 0)
		{
			this.position.setY(1024);
		}
	}
	
	public void draw(Graphics g)
	{
		Graphics2D g2d = (Graphics2D) g;
		double x = this.position.getX();
		double y = this.position.getY();
		double angle = Math.atan2(this.position.getY(), this.position.getX());
		
		
		this.Transform.setToTranslation(this.position.getX(), this.position.getY());
		
		this.Transform.rotate(angle);
		
		
		g2d.drawImage(Image, Transform, null);
		
		
	}
	
	
	
}
