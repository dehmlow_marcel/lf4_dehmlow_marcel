
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class Spielfeld extends JPanel{
	
	
	private Random rand = new Random();
	private int amountOfPlanes = 10;
	
	private ArrayList<Airplane> meineFlugzeuge = new ArrayList<Airplane>();
	/**
	 * Spielfl�che initialisieren
	 */
	public void init(){
		for(int i = 0; i < amountOfPlanes; i++)
		{
			meineFlugzeuge.add(new Airplane("","","", new Vector3D(true), new Vector3D(false), rand.nextBoolean()));
		}
	}
	
	@Override
	public void paintComponent(Graphics g){
		
		//Spielfeld zeichnen
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		
		for(Airplane t : meineFlugzeuge)
		{
			t.fly();
			t.draw(g);
		}

	}
}
