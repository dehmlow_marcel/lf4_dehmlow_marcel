import java.util.Random;

public class Vector3D {
	private double x;
	private double y;
	private double z;
	private Random rand = new Random();
	
	public Vector3D(boolean x) {
		super();
		if(x == true) // Ortsvektor
		{
		this.x = rand.nextInt(1000);
		this.y = rand.nextInt(1000);
		this.z = 0;
		}
		else// Richtungsvektor
		{
			double tempx = rand.nextDouble();
			double tempy = rand.nextDouble();
			if(tempx <= 0.5)
			{
				tempx = -tempx;
				tempx *= 5;
			}
			else
			{
				tempx *= 5;
			}
			
			if(tempy <= 0.5)
			{
				tempy = -tempy;
				tempy *= 5;
			}
			else
			{
				tempy *= 5;
			}
			
			this.x = tempx;
			this.y = tempy;
			this.z = 0;
		}
	}


	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}


	public double getZ() {
		return z;
	}


	public void setX(double x) {
		this.x = x;
	}


	public void setY(double y) {
		this.y = y;
	}


	public void setZ(double z) {
		this.z = z;
	}
	
	
	
}
