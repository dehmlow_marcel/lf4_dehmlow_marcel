

import javax.swing.JFrame;

/**
 * GUI der Klasse _______
 * @author Tenbusch
 * @version 1.0 vom 6.2.2013
 *
 */
public class SpielGUI extends JFrame {
  private Spielfeld spielfeld; 
  private final int WIDTH = 1280;
  private final int HEIGHT = 1024;
  
  //Konstruktor
  public SpielGUI(){
    super("Spiel v0.0 - FPS: ");
    
    //Fenstergr��e und Position setzen
    setSize(WIDTH, HEIGHT);
    setLocation(1,1);
    //feste Fenstergr��e
    setResizable(false);
    //Verhalten beim Schlie�en vereinbaren
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    
    //Spielfeld erstellen
    spielfeld = new Spielfeld();
    getContentPane().add(spielfeld);
    
    //sichtbar machen
    setVisible(true);
  }
  
  public void start() {

    long lastStep = System.currentTimeMillis()-1;
    
    //Spielfeld vorbereiten
    spielfeld.init();
    
    while (true){
      //FPS berechnen
      long delta = System.currentTimeMillis() - lastStep;
      lastStep = System.currentTimeMillis();
      setTitle("Spiel v0.0 - FPS: " + (1000/delta));
      
      //Spiellogik
      
      //neu zeichnen
      repaint();
      
      try{
        Thread.sleep(1);
      }catch (Exception e){
        
      }
    }

  }

}
